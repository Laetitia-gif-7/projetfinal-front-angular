import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, retry, throwError } from 'rxjs';
import { Flight } from '../model/flight';
import { VolMeteoComponent } from '../vol-meteo/vol-meteo.component';

@Injectable({
  providedIn: 'root'
})
export class VolMeteoService {


  private api_url= "https://projet-final-serveur-aero.herokuapp.com/flights"

  private api_url_owm= "https://projet-final-serveur-opw.herokuapp.com/weatherByDate"

  public vol: Flight= new Flight();

  constructor(private http: HttpClient) { }

  getVol(numeroVol : string) {
    console.log();
    
    return this.http.get<any>(`${this.api_url}/${numeroVol}`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // getMeteo(date : Date, ville : string) {
  //   return this.http.get<any>(`${this.api_url_owm}/${ville}/${new Date(date).getDate()}/${new Date(date).getMonth()}/${new Date(date).getFullYear()}/${new Date(date).getHours()}`)
  //   .pipe(
  //     retry(1),
  //     catchError(this.handleError)
  //   )
  // }

  getMeteo(date : Date, ville : string) {
    return this.http.get<any>(`${this.api_url_owm}/${ville}/${new Date(date).getDate()}/${new Date(date).getMonth()}/${new Date(date).getFullYear()}`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // getMeteoVille(ville : string) {
  //   return this.http.get<any>(`${this.api_url}/${ville}`)
  //   .pipe(
  //     retry(1),
  //     catchError(this.handleError)
  //   )
  // }

  // getMeteoDate(date : Date) {
  //   return this.http.get<any>(`${this.api_url}/${date.getDate}/${date.getMonth}/${date.getFullYear}/${date.getHours}`)
  //   .pipe(
  //     retry(1),
  //     catchError(this.handleError)
  //   )
  // }

  
  

  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }

}
