import { TestBed } from '@angular/core/testing';

import { VolMeteoService } from './vol-meteo.service';

describe('VolMeteoService', () => {
  let service: VolMeteoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VolMeteoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
