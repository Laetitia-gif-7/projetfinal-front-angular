import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VolMeteoComponent } from './vol-meteo.component';

describe('VolMeteoComponent', () => {
  let component: VolMeteoComponent;
  let fixture: ComponentFixture<VolMeteoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VolMeteoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VolMeteoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
