import { Component, OnInit } from '@angular/core';
import { Flight } from '../model/flight';
import { Weather } from '../model/weather';
import { VolMeteoService } from '../service/vol-meteo.service';

@Component({
  selector: 'app-vol-meteo',
  templateUrl: './vol-meteo.component.html',
  styleUrls: ['./vol-meteo.component.css']
})
export class VolMeteoComponent implements OnInit {

  public afficher: boolean=false; 

  public vol: Flight = new Flight();

  public meteo: Weather = new Weather();

  constructor(private volMeteoService: VolMeteoService) { }

  ngOnInit(): void {
  }

  getVol(numeroVol: string) {
    console.log(numeroVol);
    this.volMeteoService.getVol(numeroVol).subscribe({
      next: (vol) => {
        console.log(vol.flightByNumber[0]);
        this.vol = vol.flightByNumber[0];
        this.getMeteo();
        this.afficher= true;
      },
      error: (errMsg) => {
        console.log(JSON.stringify(errMsg));
      }
    })
   
  }

  getMeteo() {
    const ville = (this.vol.arrival_municipalityName).normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    console.log(ville);
    
    var date = this.vol.arrival_scheduledTimeLocal;
    console.log(date);
    
    var dateN = new Date(date)

    console.log(dateN.getDate());
    console.log(dateN.getMonth());
    console.log(dateN.getFullYear());
    console.log(dateN.getHours());
    
    
    this.volMeteoService.getMeteo(dateN, ville).subscribe({
      next: (meteo) => {
        // console.log(meteo.weathers[0]);
        // this.meteo = meteo.weathers[0];
        this.meteo = meteo.weather;
        console.log(meteo);
        
      },
      error: (errMsg) => {
        console.log(JSON.stringify(errMsg));
      }
    })
  }

// public methodeGlobale(numeroVol: string){
//   this.getVol(numeroVol);
//   this.getMeteo();
// }





  // getMeteo(date : Date, ville : string){
  //   console.log(date);
  //   console.log(ville);



  //    this.volMeteoService.getMeteo(ville, date).subscribe({
  //      next: (meteo) => { 
  //        console.log(meteo.weathers[0]);
  //        this.vol=meteo.weathers[0];
  //      },
  //      error: (errMsg) => {
  //        console.log(JSON.stringify(errMsg));
  //      }
  //    })
  //  }




}
