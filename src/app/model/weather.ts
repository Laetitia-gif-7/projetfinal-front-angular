export class Weather{
    dt!: number;
    
    temp!: number;
    
    feels_like!: number;
    
    humidity!: number;
    
    weather_id!: number;
    
    description!: string;
    
    dt_txt!: Date;
    
    city_id!: number;
    
    name!: string;
    
    country!: string;

    icon!: string;

}