export class Flight {
    departure_municipalityName!: string;
    
    departure_scheduledTimeLocal!: Date;
    
    arrival_municipalityName!: string;
    
    arrival_scheduledTimeLocal!: Date;
    
    number!: string;

    name!: string;
}