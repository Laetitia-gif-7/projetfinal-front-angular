import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { VolMeteoComponent } from './vol-meteo/vol-meteo.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr, 'fr');


@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    ConnexionComponent,
    VolMeteoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: 'accueil', component: AccueilComponent },
      { path: '', redirectTo: 'accueil', pathMatch: 'full' },
      // { path: '**', redirectTo: 'accueil', pathMatch: 'full' },
      // { path: 'connexion/:id', component: ConnexionComponent },
      { path: 'connexion', component: ConnexionComponent },
      { path: 'vol-meteo', component: VolMeteoComponent }
      
  ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
