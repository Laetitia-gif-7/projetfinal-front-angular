import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, Observable, retry, throwError } from 'rxjs';
import { User } from '../model/user';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  public userConnected = new User();

  public user = new User();
  constructor( private httpClient : HttpClient, private router : Router) { }

  ngOnInit(): void {
  }

  login(){
    this.loginServer(this.user).subscribe({
      next: (data:User) => {
         this.userConnected=data;
         this.router.navigate(['/vol-meteo']);
        },
      error: (err) => {
        console.log("une erreur" +JSON.stringify(err));
      }
    });
  }

  loginServer(user: User): Observable<User>{
    return this.httpClient.get<User>(`https://projet-final-spring.herokuapp.com/connexion/${user.username}/${user.password}`)
     .pipe(
       retry(1),
       catchError(this.handleError)
     )
   }


   handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
}
